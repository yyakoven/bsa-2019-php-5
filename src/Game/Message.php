<?php

namespace BinaryStudioAcademy\Game;

define ("RED", "\e[0;31m");
define ("LGREEN", "\e[1;32m");
define ("LBLUE", "\e[1;34m");
define ("YELLOW", "\e[1;33m");
define ("CLEAR", "\e[0m");

class Message
{
    public static function warning($message)
    {
        return RED . $message . CLEAR;
    }

    public static function info($message)
    {
        return LBLUE . $message . CLEAR;
    }

    public static function success($message)
    {
        return LGREEN . $message . CLEAR;
    }

    public static function highlight($message)
    {
        return YELLOW . $message . CLEAR;
    }   
}