<?php

namespace BinaryStudioAcademy\Game\Contracts\Helpers;

interface Math
{
    public static function luck(int $luck): bool;

    public static function damage(int $strength, int $armour): int;
}