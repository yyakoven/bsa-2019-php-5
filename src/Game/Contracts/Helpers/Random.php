<?php

namespace BinaryStudioAcademy\Game\Contracts\Helpers;

interface Random
{
    public static function get(): float;
}
