<?php

namespace BinaryStudioAcademy\Game;

class Command
{
    static public $commands = [
        'help' => ['description' => 'list available commands', 'args' => ['']],
        'stats' => ['description' => 'display current ship stats', 'args' => ['']],
        'set-sail' => ['description' => 'sail to next harbor',
            'args' => ['east', 'west', 'north', 'south']],
        'fire' => ['description' => 'attack enemy ship', 'args' => ['']],
        'aboard' => ['description' => 'collect loot from defetead ship', 'args' => ['']],
        'buy' => ['description' => 'buy one characteristic or rum',
            'args' => ['strength', 'armour', 'luck', 'rum']],
        'drink' => ['description' => 'drink rum (increases health by 30pts)', 'args' => ['']],
        'whereami' => ['description' => 'display current harbor', 'args' => ['']],
        'exit' => ['description' => 'end game', 'args' => ['']],
    ];

    public static function valid($command)
    {
        $parts = explode(" ", $command);
        if (sizeof($parts) > 2) {
            return null;
        }
        $command = $parts[0];
        $argument = $parts[1] ?? "";
        if (in_array($command, array_keys(self::$commands))
            && in_array($argument, self::$commands[$command]['args'])) {
            $command = str_replace('-', '', $command);
            return (compact('command', 'argument'));
        }
        else {
            return null;
        }
    }
}