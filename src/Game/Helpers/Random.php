<?php

namespace BinaryStudioAcademy\Game\Helpers;

use BinaryStudioAcademy\Game\Contracts\Helpers\Random as IRandom;

class Random implements IRandom
{
    public static function get(): float
    {
        return mt_rand() / mt_getrandmax();
    }

    public static function getInt($min, $max)
    {
        return (int)(self::get() * $max + $min);
    }
}
