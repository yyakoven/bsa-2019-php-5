<?php

namespace BinaryStudioAcademy\Game\Helpers;

use BinaryStudioAcademy\Game\Contracts\Helpers\Math as IMath;
use BinaryStudioAcademy\Game\Helpers\Random;

class Math implements IMath
{
    private const DAMAGE_COEFFICIENT = 10;

    public static function luck(int $luck): bool
    {
        return ceil(Random::get() * Stats::MAX_LUCK) > (Stats::MAX_LUCK - $luck);
    }

    public static function damage(int $strength, int $armour): int
    {
        return (int)ceil(self::DAMAGE_COEFFICIENT * $strength / $armour);
    }
}
