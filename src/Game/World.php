<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Ships\PirateShip;
use BinaryStudioAcademy\Game\Harbors\PiratesHarbor;
use BinaryStudioAcademy\Game\Factories\HarborFactory;
use BinaryStudioAcademy\Game\Message;

class World
{
    private $player;
    private $harbor;
    private $factory;

    public function __construct()
    {
        $this->player = new PirateShip();
        $this->harbor = new PiratesHarbor(null);
    }
    public function fire()
    {
        $player = $this->player;
        $enemy = $this->harbor->ship;

        if ($this->harbor->isPirateHarbor()) {
            return Message::info("there are no enemies here");
        }
        if (! $enemy->isAlive()) {
            return Message::info("enemy has been defeated");
        }
        $message = $player->attack($enemy) . PHP_EOL;
        if ($enemy->isAlive()) {
            $message .= $enemy->attack($player);
        } else if ($enemy->name == "HMS Royal Sovereign") {
            exit(Message::success("You defeated the last enemy. Congratulations."));
        }
        if ( ! $player->isAlive()) {
            exit(Message::warning("your ship was destroyed by the royal fleet\n"));
        }
        return Message::success($message);
    }

    public function drink()
    {
        if ($this->player->hasRum()) {
            return Message::success($this->player->drinkRum());
        }
        return Message::warning("you do not have any rum, but i bet the royal fleet does");
    }

    public function help()
    {
        $info = Message::highlight("Available Commands:" . PHP_EOL);
        foreach(Command::$commands as $command => $about)
        {
            $arguments = implode(' | ', $about['args']);
            $info .= Message::info("  " . sprintf("%-10s", $command));
            $info .= ($arguments ? (Message::info("{$arguments} ")) : '');
            $info .= $about['description'];
            $info .= PHP_EOL;
        }
        return $info;
    }

    public function stats($ship = null)
    {
        $ship = $ship ? $ship : $this->player;
        $stats = '';
        $props = ['health', 'strength', 'armour', 'luck', 'hold'];
        foreach($props as $prop)
        {
            $stats .= Message::info(sprintf("%-10s", "{$prop}:"));
            $stats .= sprintf("%11s\n", $ship->$prop);
        }
        return $stats;
    }

    public function enemystats()
    {
        $ship = $this->harbor->ship;
        echo gettype($this->harbor->ship);
        return ($this->stats($ship));
    }

    public function aboard()
    {
        $player = $this->player;
        $enemy = $this->harbor->ship;

        if (! $enemy) {
            return Message::info("there are no enemies here");
        }
        if( $enemy->isAlive()) {
            return Message::warning("you have to defeat the enemy first");
        }
        if ($player->inventoryIsFull()) {
            return Message::warning("inventory is full");
        }
        if ( !($item = $enemy->removeLootItem())) {
            return Message::warning("enemy ship is empty");
        } else {
            $player->addLoot($item);
            return Message::success($item . " was retrived from the enemy ship");
        }
        // return ($this->harbor->ship->hold);
        return "todo: gather loot from the enemy ship";
    }

    public function whereami()
    {
        return Message::info($this->harbor->name);
    }

    public function setsail($direction)
    {
        $id = $this->harbor->$direction;
        if ($id) {
            $factory = HarborFactory::getFactory($id);
            $this->factory = new $factory($id);
            $this->harbor = $this->factory->createHarbor($id);
            if($this->player->health < 60 && $id == 1) {
                $this->player->fillHealth();
            }
            return "sailing " . Message::info($direction) . ", destinaion - "
            . Message::info($this->harbor->name);
        }
        else {
            return Message::warning("there's no harbor there");
        }
    }

    public function buy($item)
    {
        if ( ! $this->harbor->isPirateHarbor()) {
            return Message::warning("you can only make purchases at the Pirates Harbor");
        }
        if ( ! $this->player->hasGold()) {
            return Message::warning("you are out of gold");
        }
        return Message::success($this->player->buy($item));
    }

    public function exit($message = null)
    {
        exit("exiting the game..\n");
    }

    public function do($command)
    {
        $props = Command::valid($command);
        if ($props) {
            $argument = $props['argument'];
            $command = $props['command'];
            return $this->$command($argument);
        } else {
            return Message::warning("no such command, try again");
        }
    }
}