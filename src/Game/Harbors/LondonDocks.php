<?php

namespace BinaryStudioAcademy\Game\Harbors;

class LondonDocks extends Harbor
{
    protected $name = "London Docks";
    protected $id = 8;

    protected $north = 6;
    protected $south = 0;
    protected $east = 0;
    protected $west = 7;
}