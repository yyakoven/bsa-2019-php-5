<?php

namespace BinaryStudioAcademy\Game\Harbors;

class Felixtowe extends Harbor
{
    protected $name = "Felixtowe";
    protected $id = 7;

    protected $north = 5;
    protected $south = 0;
    protected $east = 8;
    protected $west = 2;
}