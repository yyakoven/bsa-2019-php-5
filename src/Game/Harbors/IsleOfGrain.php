<?php

namespace BinaryStudioAcademy\Game\Harbors;

class IsleOfGrain extends Harbor
{
    protected $name = "Isle of Grain";
    protected $id = 5;

    protected $north = 0;
    protected $south = 7;
    protected $east = 6;
    protected $west = 4;
}