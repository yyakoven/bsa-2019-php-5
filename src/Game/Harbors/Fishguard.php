<?php

namespace BinaryStudioAcademy\Game\Harbors;

class Fishguard extends Harbor
{
    protected $name = "Fishguard";
    protected $id = 3;

    protected $north = 4;
    protected $south = 2;
    protected $east = 1;
    protected $west = 0;
}