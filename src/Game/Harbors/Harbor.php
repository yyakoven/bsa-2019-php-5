<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Ships\Ship;

abstract class Harbor
{
    protected $name;
    protected $id;

    protected $north;
    protected $south;
    protected $east;
    protected $west;
    
    protected $ship;

    public function __construct(?Ship $ship)
    {
        $this->ship = $ship;
    }

    public function __get($property)
    {
        return $this->$property;
    }

    public function isPirateHarbor()
    {
        return $this->id == 1;
    }
}
