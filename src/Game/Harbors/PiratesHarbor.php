<?php

namespace BinaryStudioAcademy\Game\Harbors;

class PiratesHarbor extends Harbor
{
    public $name = "Pirates Harbor";
    protected $id = 1;

    protected $north = 4;
    protected $south = 2;
    protected $east = 0;
    protected $west = 3;
}