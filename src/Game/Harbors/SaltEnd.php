<?php

namespace BinaryStudioAcademy\Game\Harbors;

class SaltEnd extends Harbor
{
    protected $name = "Salt End";
    protected $id = 4;

    protected $north = 0;
    protected $south = 1;
    protected $east = 5;
    protected $west = 3;
}