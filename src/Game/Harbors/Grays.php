<?php

namespace BinaryStudioAcademy\Game\Harbors;

class Grays extends Harbor
{
    protected $name = "Grays";
    protected $id = 6;

    protected $north = 0;
    protected $south = 8;
    protected $east = 0;
    protected $west = 5;
}