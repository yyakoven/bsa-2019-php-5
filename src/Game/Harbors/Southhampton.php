<?php

namespace BinaryStudioAcademy\Game\Harbors;

class Southhampton extends Harbor
{
    protected $name = "Southhampton";
    protected $id = 2;

    protected $north = 1;
    protected $south = 0;
    protected $east = 7;
    protected $west = 3;
}