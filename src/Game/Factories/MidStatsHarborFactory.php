<?php

namespace BinaryStudioAcademy\Game\Factories;

use BinaryStudioAcademy\Game\Factories\HarborFactory;
use BinaryStudioAcademy\Game\Harbors\Harbor;
use BinaryStudioAcademy\Game\Ships\RoyalBattleShip;

class MidStatsHarborFactory extends HarborFactory
{
    public function createHarbor($id): Harbor
    {
        $ship = new RoyalBattleShip();
        $harbor = new $this->harbors[$id]($ship);
        return $harbor;
    }
}