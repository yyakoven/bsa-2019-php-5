<?php

namespace BinaryStudioAcademy\Game\Factories;

use BinaryStudioAcademy\Game\Factories\HarborFactory;
use BinaryStudioAcademy\Game\Harbors\Harbor;

class PiratesHarborFactory extends HarborFactory
{
    public function createHarbor($id): Harbor
    {
        $harbor = new $this->harbors[$id](null);
        return $harbor;
    }
}