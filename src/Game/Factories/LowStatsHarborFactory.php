<?php

namespace BinaryStudioAcademy\Game\Factories;

use BinaryStudioAcademy\Game\Factories\HarborFactory;
use BinaryStudioAcademy\Game\Harbors\Harbor;
use BinaryStudioAcademy\Game\Ships\RoyalPatroolSchooner;

class LowStatsHarborFactory extends HarborFactory
{
    public function createHarbor($id): Harbor
    {
        $ship = new RoyalPatroolSchooner();
        $harbor = new $this->harbors[$id]($ship);
        return $harbor;
    }
}