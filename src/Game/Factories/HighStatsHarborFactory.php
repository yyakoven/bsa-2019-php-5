<?php

namespace BinaryStudioAcademy\Game\Factories;

use BinaryStudioAcademy\Game\Factories\HarborFactory;
use BinaryStudioAcademy\Game\Harbors\Harbor;
use BinaryStudioAcademy\Game\Ships\HMSRoyalSovereign;

class HighStatsHarborFactory extends HarborFactory
{
    public function createHarbor($id): Harbor
    {
        $ship = new HMSRoyalSovereign();
        $harbor = new $this->harbors[$id]($ship);
        return $harbor;
    }
}