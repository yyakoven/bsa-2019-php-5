<?php

namespace BinaryStudioAcademy\Game\Factories;

use BinaryStudioAcademy\Game\Harbors\Harbor;

abstract class HarborFactory
{
    protected $harbors = [
        1 => '\BinaryStudioAcademy\Game\Harbors\PiratesHarbor',
        2 => '\BinaryStudioAcademy\Game\Harbors\Southhampton',
        3 => '\BinaryStudioAcademy\Game\Harbors\Fishguard',
        4 => '\BinaryStudioAcademy\Game\Harbors\SaltEnd',
        5 => '\BinaryStudioAcademy\Game\Harbors\IsleOfGrain',
        6 => '\BinaryStudioAcademy\Game\Harbors\Grays',
        7 => '\BinaryStudioAcademy\Game\Harbors\Felixtowe',
        8 => '\BinaryStudioAcademy\Game\Harbors\LondonDocks'
    ];

    static public function getFactory($id)
    {
        if ($id == 1) {
            return '\BinaryStudioAcademy\Game\Factories\PiratesHarborFactory';
        }
        else if ($id >= 2 && $id <= 5) {
            return '\BinaryStudioAcademy\Game\Factories\LowStatsHarborFactory';
        }
        else if ($id == 6 || $id == 7) {
            return '\BinaryStudioAcademy\Game\Factories\MidStatsHarborFactory';
        }
        else {
            return '\BinaryStudioAcademy\Game\Factories\HighStatsHarborFactory';
        }
    }

    abstract public function createHarbor($id): Harbor;
}