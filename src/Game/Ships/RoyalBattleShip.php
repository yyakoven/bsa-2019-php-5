<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Helpers\Random;

class RoyalBattleShip extends Ship
{
    public function __construct()
    {
        $this->strength = Random::getInt(4, 8);
        $this->armour = Random::getInt(4, 8);
        $this->luck = Random::getInt(4, 7);
        $this->health = 80;
        $this->hold = ["\u{1F37E}"];
        $this->name = "Royal Battle Ship";
    }
}