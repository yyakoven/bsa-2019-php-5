<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Helpers\Random;

class RoyalPatroolSchooner extends Ship
{
    public function __construct()
    {
        $this->strength = Random::getInt(2, 4);
        $this->armour = Random::getInt(2, 4);
        $this->luck = Random::getInt(1, 4);
        $this->health = 50;
        $this->hold = ["\u{1F4B0}"];
        $this->name = 'Royal Patrool Schooner';
    }
}