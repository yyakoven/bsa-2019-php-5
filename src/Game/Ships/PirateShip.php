<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Harbors\Harbor;

class PirateShip extends Ship
{
    private $location;

    public function __construct()
    {
        $this->strength = 4;
        $this->armour = 4;
        $this->luck = 4;
        $this->health = 60;
        $this->hold = ["\u{1F37E}"];
        $this->name = "Pirate Ship";
    }

    public function setLocation(Harbor $harbor)
    {
        $this->location = $harbor;
    }

    public function location()
    {
        return $this->location;
    }

    public function hasRum()
    {
        return in_array("\u{1F37E}", $this->hold);
    }

    public function hasGold()
    {
        return in_array("\u{1F4B0}", $this->hold);
    }

    public function drinkRum()
    {
        if ($this->health < 100) {
            foreach ($this->hold as $key => $value) {
                if ($value == "\u{1F37E}") {
                    unset($this->hold[$key]);
                    $this->increaseHealth();
                    break;
                }
            }
            return "you drank some rum, your health has increased";
        }
        else {
            return "your health is at its maximum";
        }
    }

    public function spendGold()
    {
        foreach ($this->hold as $key => $value) {
            if ($value == "\u{1F4B0}") {
                unset($this->hold[$key]);
                break;
            }
        }
        return "your purchase was successfull";
    }

    public function increaseHealth()
    {
        $health = $this->health + 10; 
        $this->health = $health > 100 ? 100 : $health;
    }

    public function fillHealth()
    {
        $this->health = 60;
    }

    public function inventoryIsFull()
    {
        return sizeof($this->hold) >= 3;
    }

    public function buyRum()
    {
        $this->hold[] = "\u{1F37E}";
        return $this->spendGold();
    }

    public function buyTrait($trait)
    {
        if ($this->$trait < 10) {
            $this->$trait += 1;
            return $this->spendGold() 
            . ", your $trait has been increased";
        }
        else {
            return "your $trait is at its maximum, so it can't be increased";
        }
    }

    public function buy($item)
    {
        if ($item == 'rum') {
            return $this->buyRum();
        }
        else {
            return  $this->buyTrait($item);
        }
    }

    public function addLoot($item)
    {
        $this->hold[] = $item;
    }
}
