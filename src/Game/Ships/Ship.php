<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Helpers\Random;
use BinaryStudioAcademy\Game\Helpers\Math;

abstract class Ship
{
    protected $strength;
    protected $armour;
    protected $luck;
    protected $health;
    protected $hold;
    protected $name;

    public function __get($property)
    {
        if ($property == 'hold') {
            $hold = '[';
            foreach ($this->hold as $item) {
                $hold .= " {$item} ";
            }
            $hold .= ']';
            return $hold;
        }
        return $this->$property;
    }

    public function attack($enemy)
    {
        if (Math::luck($this->luck)) {
            $damage = Math::damage($this->strength, $enemy->armour);
            $health = $enemy->health - $damage;
            if ($health > 0) {
                $enemy->health = $health;
                return "{$enemy->name} has been damaged by {$this->name} and lost {$damage} health points";
            }
            $enemy->health = 0;
            return "{$enemy->name} has been defeated";
        }
        return "{$this->name} missed his target";
    }

    public function isAlive()
    {
        return $this->health > 0;
    }

    public function removeLootItem()
    {
        return array_shift($this->hold);
    }
}