<?php

namespace BinaryStudioAcademy\Game\Ships;

class HMSRoyalSovereign extends Ship
{
    public function __construct()
    {
        $this->strength = 10;
        $this->armour = 10;
        $this->luck = 10;
        $this->health = 100;
        $this->hold = ["\u{1F4B0}", "\u{1F4B0}", "\u{1F37E}"];
        $this->name = "HMS Royal Sovereign";
    }
}