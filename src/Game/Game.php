<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class Game
{
    private $world;

    public function __construct()
    {
        $this->world = new World();
    }

    public function start(Reader $reader, Writer $writer)
    {
        while (true) {
            $writer->write("» ");
            $command = trim($reader->read());
            $writer->writeln(PHP_EOL . $this->world->do($command) . PHP_EOL);
        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        $writer->writeln('This method runs program step by step.');
    }
}
